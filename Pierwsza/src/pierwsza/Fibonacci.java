package pierwsza;

/**
 * Obiekt generujący ciąg fibonacciego
 * @author Paweł Rynowiecki
 */
public class Fibonacci {
    private long[] ciag;
    private int max;
    private int indeks = 0;
    public Fibonacci(int i){
        ciag = new long [i];
        max = i;
        Generuj();
    }
    public long Ciag(int i){
        if (i < indeks)
            return ciag[i];
        else
            return 0;
    }
    private void Dodaj(int i){
        ciag[indeks] = i;
        indeks++;
    }
    private void Generuj(){
        int i=1,j=1,k;
        Dodaj(i); Dodaj(j);
        while(indeks < max){
            k=i+j;
            Dodaj(k);
            i=j; j=k;
        }
    }
}
