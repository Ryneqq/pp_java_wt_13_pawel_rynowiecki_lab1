package pierwsza;
import java.io.*;

/**
 * Czyta plik o zadanej nazwie w folderze 'files'
 * @author Paweł
 */
public class Czytaj {
    public String Plik(String nazwa){
        String read=null, line=null;
        //Tworzymy nowy plik i sprawdzamy jego sciezke bezwzgledna
        String path = new File("").getAbsolutePath();
        // Teraz ze sciezka dostajemy sie do pliku ktory znajduje sie
        // w folderze files
        File file = new File(path + "/files/" + nazwa);
        // Lapiemy wyjatki i zczytujemy co bylo w pliku
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            while((line=in.readLine())!= null){
                if(read == null)
                    read = line;
                else
                    read+=line;
            }
            in.close();
        } catch (FileNotFoundException ex) {
            System.err.println("Dupa, nie udało się otworzyc");
        } catch (IOException ex2) {
            System.err.println("Ojojoj tego nie wyklepiesz");
        }
        // zwroc to co przeczytales
        return read;
    }
}
