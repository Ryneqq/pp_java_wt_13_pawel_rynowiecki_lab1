
package pierwsza;
import pierwsza.poker.*;

/**
 * Obiekt obsługujący klasy wykonujace zadania z pierwszej listy
 * @author Paweł Rynowiecki
 */
public class Pierwsza {

    public static void Slowa(String slowa){
        String Slowo;
        char[] slowo;
        //Trojkaty t= new Trojkaty();
        int max = slowa.lastIndexOf('"');
        int ileT=0;
        
        for (int i=0;i<max;i++){
            //System.out.println(slowa.charAt(i));
            if(slowa.charAt(i)=='"'){
                i++;
                //licze jak duza tablice zrobic
                for(int j=i,k=0; ; j++,k++){
                    if(slowa.charAt(j)=='"'){
                        slowo = new char [k];
                        break;
                    }
                }
                //kopiuje zawartosc slowa do tablicy
                for(int j=0; slowa.charAt(i)!='"'; j++, i++ ){
                        slowo[j]=slowa.charAt(i);
                }

                Slowo = String.valueOf(slowo); //rzutowanie na string
                Slowo = Slowo.toLowerCase(); //male litery
                //System.out.println(Slowo);
                Trojkaty t= new Trojkaty();
                if(t.Sprawdz(Slowo)) //czy nasze slowo jest trojkatne?
                    ileT++; //owszem
            }
        }
        System.out.format("liczba trojkatow: %d%n",ileT);
    }
    
    public static void SumaFib(){
        Fibonacci fib = new Fibonacci(50);
        long suma=0;
        
        for(int i=0; i < 100; i++)
            if((fib.Ciag(i)%2) == 0)
                if(fib.Ciag(i) < 4000000 && fib.Ciag(i) > 0)
                    suma+=fib.Ciag(i);
                else
                    break;
        //System.out.println(suma);
        System.out.format("suma z ciagu Fibonacciego: %d%n",suma);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //SumaFib();
        Czytaj Odczytaj = new Czytaj();
        //Slowa(Odczytaj.Plik("slowa.txt"));*/
        /*Karta k = new Karta();
        k.Dodaj("KH");
        System.out.format("%d %d %n",k.Wartosc(),k.Kolor());*/
        //Partia p = new Partia();
        //p.Rozdanie("3H 3D 4H 4C 7D 4S 4D 3S 7H 3C");
        //Trojkaty t = new Trojkaty();
        //System.out.println(t.Kolejnosc('t'));
        Poker poker = new Poker();
        poker.Gra(Odczytaj.Plik("pok3.txt"));
        

    
    }
    
}
