package pierwsza;
import java.lang.Character;
/**
 *
 * @author Paweł
 */

public class Trojkaty {
    public int Kolejnosc(char a){
        return java.lang.Character.getNumericValue(a);
    }
    public boolean Sprawdz(String slowo){
        int suma=0,t;
        
        for (int i=0; i < slowo.length(); i++){
            suma += Kolejnosc(slowo.charAt(i));
            suma -= 9;
        }
        //System.out.println(suma);
        for(int i=1; ;i++){
            t = (int)(0.5*i*(i+1));
            //System.out.println(t);
            if (suma == t)
                return true;
            if (suma < t){
                return false;
            }
        }
    }
    
}
