/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pierwsza.poker;

/**
 *
 * @author Paweł
 */
public class Gracz {
    Karta[] reka;
    int wygrana = 0;
    int indeks = 0;
    public Gracz(){
        reka = new Karta[5];
        Karta temp = new Karta();
        for(int i=0; i<5; i++)
            reka[i] = temp;
    }
    public void Wez(Karta k){
        if(indeks < 5){
            reka[indeks] = k;
            indeks++;
        }
    }
    public Karta Reka(int i){
        if(i < 5)
            return reka[i];
        else
            return null;
    }
    public void Zwyciestwo(){
        wygrana++;
        indeks = 0;
    }
    public int Wygrana(){
        return wygrana;
    }
}
