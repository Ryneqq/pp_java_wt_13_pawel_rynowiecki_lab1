/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pierwsza.poker;

/**
 *
 * @author Paweł
 */
public class Karta implements Cloneable {
    String nazwa;
    int wartosc;
    int kolor;
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    private int Kolejnosc(char a){
        return java.lang.Character.getNumericValue(a);
    }
    public void Dodaj(String n){
        nazwa = n;
        NadajWartosc();
        NadajKolor();
    }
    private void NadajWartosc(){
        if(Kolejnosc(nazwa.charAt(0))> 1 && Kolejnosc(nazwa.charAt(0)) < 10)
            wartosc = Kolejnosc(nazwa.charAt(0));
        else if(Kolejnosc(nazwa.charAt(0)) == 1 && Kolejnosc(nazwa.charAt(0)) == 29) // 10
            wartosc = 10;
        else if(Kolejnosc(nazwa.charAt(0)) == 10) // A
            wartosc = 14;
        else if(Kolejnosc(nazwa.charAt(0)) == 13 && Kolejnosc(nazwa.charAt(0)) == 26) // D
            wartosc = 12;
        else if(Kolejnosc(nazwa.charAt(0)) == 19) // J
            wartosc = 11;
        else if(Kolejnosc(nazwa.charAt(0)) == 20) // K
            wartosc = 13;
        
    }
    private void NadajKolor(){
        // 1 - pik, 2- trefl, 3 - karo, 4- kier
        int m=1;
        if(nazwa.charAt(m)=='0')
            m++;
        
        if(nazwa.charAt(m)=='S')
            kolor = 1;
        else if(nazwa.charAt(m)=='C')
            kolor = 2;
        else if(nazwa.charAt(m)=='D')
            kolor = 3;
        else if(nazwa.charAt(m)=='H')
            kolor = 4;
        
    }
    public int Wartosc(){
        return wartosc;
    }
    public int Kolor(){
        return kolor;
    }
}
