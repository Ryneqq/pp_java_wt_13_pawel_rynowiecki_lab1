/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pierwsza.poker;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paweł
 */
public class Partia {
    Gracz[] gracze;
    public Partia(){
        Gracz temp = new Gracz();
        Gracz temp2 = new Gracz();
        gracze = new Gracz[2];
        gracze[0] = temp;
        gracze[1] = temp2;
    }
    public void Wyswietl(){
        for(int i=0; i<2; i++){
            System.out.format("Gracz numer %d: | ", i+1);
            for(int j=0; j<5; j++){
                System.out.format("%d %d | ",gracze[i].Reka(j).Wartosc(),gracze[i].Reka(j).Kolor());
            }
            System.out.print("\n");
        }
    }
    public int Rozdanie(String r){
        // przydziel karty do graczy i wywolaj metode sprawdzajaca figury
        Karta karta = new Karta();
        char[] buf;
        String nazwa;
        
        for(int i=0, j=0, k=0; i < r.length(); i++){
            if(k == 5){
                j++;
                if(j == 2)
                    break;
                k=0;
            }
            if(r.charAt(i) == '1'){
                buf = new char[3];
                buf[0]='1';
                buf[1]='0';
                i+=2;
                buf[2]=r.charAt(i);
                nazwa = String.valueOf(buf);
                karta.Dodaj(nazwa);
                try {
                    gracze[j].Wez((Karta)karta.clone());
                } catch (CloneNotSupportedException ex) {
                    System.out.println("Cos zlapalem!");
                }
                k++;
            } else if (r.charAt(i) != ' '){
                buf = new char[2];
                buf[0]=r.charAt(i);
                ++i;
                buf[1]=r.charAt(i);
                nazwa = String.valueOf(buf);
                karta.Dodaj(nazwa);
                try {
                    gracze[j].Wez((Karta)karta.clone());
                } catch (CloneNotSupportedException ex) {
                    System.out.println("Cos zlapalem!");
                }
                k++;
                //System.out.format("%d %d ",karta.Wartosc(),karta.Kolor());
            }
        }
        Wyswietl();
        return Wygrany();
    }
    private int Figura(Gracz g){
        int para=1;
        for(int i=0;i<5;i++){
            for(int j=i; j < 4; j++){
                if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                 //znaleziono pare
                    para++;
                }
            }
        }
        int wartosc=0;
        boolean kolor = true, strit = false;
        int wK=0, wKs=0,s = 1;
        
        switch(para){
        case 1:
            // nie ma pary
            // szukaj strita/koloru
            // jak niema, zwroc największa karte
            for(int i=1;i<5;i++){ //kolor?
                if(g.Reka(0).Kolor() != g.Reka(i).Kolor()){
                    kolor = false;
                    break;
                }
            }
            for(int i=0;i<5;i++){ //znajdz najwyzsza karte
                if (wK < g.Reka(i).Wartosc())
                    wK = g.Reka(i).Wartosc();
            }
            if (wK != 14){ 
                for(int i=1;i<5;i++){
                    for(int j=0; j < 5; j++){
                        if(g.Reka(j).Wartosc() == wK-i){
                            s++;
                            break;
                        }
                    }
                }
                if (s == 5){
                    strit = true;
                }
                
            } else {
                for(int j=0; j < 5; j++){
                        if(g.Reka(j).Wartosc() == 2){
                            wKs = 5;
                            break;
                        }
                    }
                for(int i=1;i<4;i++){
                    for(int j=0; j < 5; j++){
                        if(g.Reka(j).Wartosc() == wKs-i){
                            s++;
                            break;
                        }
                    }
                }
                if (s == 4){
                    strit = true;
                }
            }
            if (strit || kolor){
                if(strit && kolor){ //poker
                    if(wK == 14 && wKs == 0) //krolewski
                        wartosc = 1000 * g.Reka(0).Kolor();
                    else if (wKs != 0){ // najgorszy
                        wartosc = 900 + wKs;
                    } else {
                        wartosc = 900 + wK;
                    }
                } else if (kolor){
                    wartosc = 500 + wK;
                } else {
                    if(wKs != 0)
                        wartosc = 400 + wKs;
                    else {
                        wartosc = 400 + wK;
                    }
                }
            } else {
                wartosc = wK; // wartosc najwyzszej karty - w rece nie mamy nic
            }
            
        break;
        case 2:
            // jest jedna para
            for(int i=0;i<5;i++){
                for(int j=i; j < 4; j++){
                    if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                        wartosc = 100 + g.Reka(i).Wartosc();
                        break;
                    }
                }
            }
        break;
        case 3:
            // sa 2 pary
            int p=0;
            for(int i=0;i<5;i++){
                for(int j=i; j < 4; j++){
                    if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                        if (p==0)
                            p=g.Reka(i).Wartosc();
                        else if (p < g.Reka(i).Wartosc())
                            p=g.Reka(i).Wartosc();
                    }
                }
            }
            wartosc = 200 + p;
        break;
        case 4:
            // jest trojka
            for(int i=0;i<5;i++){
                for(int j=i; j < 4; j++){
                    if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                        wartosc = 300 + g.Reka(i).Wartosc();
                        break;
                    }
                }
            }
        break;
        case 5:
            // jest full
            int f=1;
            for(int i=0;i<4;i++){
                if(g.Reka(0).Wartosc() == g.Reka(i+1).Wartosc()){
                    f++;
                }
            }
            if (f==3){
                wartosc = 600 + g.Reka(0).Wartosc();
            } else {
                for(int i=0;i<4;i++){
                    if(g.Reka(0).Wartosc() != g.Reka(i+1).Wartosc()){
                        wartosc = 600 + g.Reka(i+1).Wartosc();
                    }
            }
            }
        break;
        case 7:
            // jest kareta
            for(int i=0;i<5;i++){
                for(int j=i; j < 4; j++){
                    if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                        wartosc = 800 + g.Reka(i).Wartosc();
                        break;
                    }
                }
            }
        break;
        default:
        break;
        }
        // na podstawie wartosci zmiennej para bede wstanie okreslic
        // z czym ma do czynienia
        
        return wartosc; // nie znaleziono nic - wystapil blad;
    }
    public int Nkarta(Gracz g){
        int max = 0;
        for(int i=0;i<5;i++){
            if(g.Reka(i).Wartosc() > max){
                max = g.Reka(i).Wartosc();
            }
        }
        return max;
    }
    public int KolorNK(Gracz g){
        int max = 0, k = 0;
        for(int i=0;i<5;i++){
            if(g.Reka(i).Wartosc() > max){
                max = g.Reka(i).Wartosc();
                k = g.Reka(i).Kolor();
            }
        }
        return k;
    }
    public int NPara(Gracz g){
        int p=0;
        for(int i=0;i<5;i++){
            for(int j=i; j < 4; j++){
                if(g.Reka(i).Wartosc() == g.Reka(j+1).Wartosc()){
                    if (p==0)
                        p=g.Reka(i).Wartosc();
                    else if (p > g.Reka(i).Wartosc())
                        p=g.Reka(i).Wartosc();
                }
            }
        }
        return p;
    }
    public int Wygrany(){
        int g1,g2;
        Karta k = new Karta();
        k.Dodaj("2C");
        g1 = Figura(gracze[0]);
        g2 = Figura(gracze[1]);
        if(g1 > g2){
            System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
            return 1;
        } else if(g1 < g2){
            System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g2);
            return 2;
        } else {
            if (g1 > 200 && g1 < 300){
                //mamy dwojeczke, trzeba sprawdzic najpierw druga pare
                if(NPara(gracze[0]) > NPara(gracze[1])){
                    System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
                    return 1;
                } else if(NPara(gracze[0]) < NPara(gracze[1])){
                    System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g2);
                    return 2;
                } else { // jednak decyduje najwieksza karta
                    if(Nkarta(gracze[0]) > Nkarta (gracze[1])){
                        System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
                        return 1;  
                    } else if (Nkarta(gracze[0]) < Nkarta (gracze[1])){
                        System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g1);
                        return 2;    
                    } else { //najwyzsze karty obu graczy sa identyczne, rozstrzyga kolor
                        if(KolorNK(gracze[0]) > KolorNK (gracze[1])){
                            System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
                            return 1;  
                        } else {
                            System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g1);
                            return 2;   
                        }
                    }
                }   
            } else { // szukamy najwiekszej karty
                if(Nkarta(gracze[0]) > Nkarta (gracze[1])){
                    System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
                    return 1;  
                } else if (Nkarta(gracze[0]) < Nkarta (gracze[1])){
                    System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g1);
                    return 2;    
                } else { //najwyzsze karty obu graczy sa identyczne, rozstrzyga kolor
                    if(KolorNK(gracze[0]) > KolorNK (gracze[1])){
                        System.out.format("Gracz 1 wygrywa partie z wynikiem: %d%n",g1);
                        return 1;  
                    } else {
                        System.out.format("Gracz 2 wygrywa partie z wynikiem: %d%n",g1);
                        return 2;   
                    }
                }
            }
        }
    }
}
